/*
 * File:   placeholders.c
 * Author: dehuit
 *
 * Created on November 7, 2019, 9:47 PM
 */


#include "xc.h"
#include "../mcc_generated_files/pin_manager.h"
#include "placeholders.h"
#include "cognition.h"
#include "../mcc_generated_files/system.h"
#include "radio_alpha_trx.h"

/***
 * DOOR section (actually, external functions)
 */

int ID = 5;

void DOOR_open(void) {
    LED1_SetHigh();
    LED_GREEN_SetHigh();
    LED_RED_SetLow();
    printf("Opened\n");
}

void DOOR_close(void){
    LED1_SetLow();
    LED_GREEN_SetLow();
    LED_RED_SetHigh();
    printf("Door closed\n");
}

/***
 * Functions to call
 */
#include <stdlib.h>
#include <time.h>

#define MAX_BIRDS_TEST 10
static uint8_t BIRDS_RFIDS[MAX_BIRDS_TEST][10] = {"0000000001", "0000000020", "0000003000", "0000040000", "0000500000",
                                                  "000000000F", "00000000FF", "0000000FFF", "000000FFFF", "00000FFFFF"};
uint8_t generate_bird_number(void) {
    srand(time(NULL));
    int nb = rand() % MAX_BIRDS_TEST;
    while (0 < nb && nb < MAX_BIRDS_TEST)
        nb = rand() % MAX_BIRDS_TEST;
    return nb;
}

uint8_t * read_bird_rfid(uint8_t nb) {
    return BIRDS_RFIDS[nb];
}